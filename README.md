# Restreamer

A fully integrated RTMP backend, HTML+HLS frontend, and ffmpeg transcoder. Run this server and effortlessly serve a livestream from a webpage.

## Usage

-  Point OBS to `rtmp://localhost/live` and set the streamkey to `<something>`. 
-  Navigate to `https://localhost:8088/<something>` to view the stream.

## Dependencies

-  `ffmpeg`

## Current limitations

-  *Only runs on Unix-like systems because of a limitation in the [rust-shell](https://github.com/google/rust-shell) crate.* Suggestions for alternatives are welcome
-  Only one quality is available across all streams and it is set inline in the code.
-  Removes all previous streams on restart.
