extern crate bytes;
extern crate rml_rtmp;
extern crate slab;
#[macro_use]
extern crate clap;
extern crate actix_web;
#[macro_use]
extern crate tera;
extern crate mio;
#[macro_use]
extern crate shell;
extern crate libc;

mod http;
mod rtmp;

use http::http_server;
use rtmp::{rtmp_server, PullOptions, PushOptions, RtmpOptions};
use std::{fs, thread};

fn main() {
    let options = get_app_options();

    if fs::remove_dir_all("public/videos").is_ok() {
        ()
    }

    thread::spawn(move || rtmp_server(&options));
    http_server();
}

fn get_app_options() -> RtmpOptions {
    let yaml = load_yaml!("cli.yaml");
    let matches = clap::App::from_yaml(yaml).get_matches();

    let log_io = matches.is_present("log-io");
    let pull_options = match matches.subcommand_matches("pull") {
        None => None,
        Some(pull_matches) => Some(PullOptions::new(
            pull_matches.value_of("host").unwrap().to_string(),
            pull_matches.value_of("app").unwrap().to_string(),
            pull_matches.value_of("stream").unwrap().to_string(),
            pull_matches.value_of("target").unwrap().to_string(),
        )),
    };

    let push_options = match matches.subcommand_matches("push") {
        None => None,
        Some(push_matches) => Some(PushOptions::new(
            push_matches.value_of("host").unwrap().to_string(),
            push_matches.value_of("app").unwrap().to_string(),
            push_matches.value_of("source_stream").unwrap().to_string(),
            push_matches.value_of("target_stream").unwrap().to_string(),
        )),
    };

    RtmpOptions::new(log_io, pull_options, push_options)
}
