mod connection;
mod server;

use mio::net::{TcpListener, TcpStream};
use mio::*;
use rtmp::connection::{Connection, ConnectionError, ReadResult};
use rtmp::server::{Server, ServerResult};
use slab::Slab;
use std::{collections::HashSet, net::SocketAddr, str::FromStr, time::SystemTime, usize};

const SERVER: Token = Token(usize::MAX - 1);

type ClosedTokens = HashSet<usize>;

enum EventResult {
    None,
    ReadResult(Box<ReadResult>),
    DisconnectConnection,
}

#[derive(Debug)]
pub struct PullOptions {
    host: String,
    app: String,
    stream: String,
    target: String,
}

impl PullOptions {
    pub fn new(host: String, app: String, stream: String, target: String) -> PullOptions {
        PullOptions {
            host,
            app,
            stream,
            target,
        }
    }
}

#[derive(Debug)]
pub struct PushOptions {
    host: String,
    app: String,
    source_stream: String,
    target_stream: String,
}

impl PushOptions {
    pub fn new(
        host: String,
        app: String,
        source_stream: String,
        target_stream: String,
    ) -> PushOptions {
        PushOptions {
            host,
            app,
            source_stream,
            target_stream,
        }
    }
}

#[derive(Debug)]
pub struct RtmpOptions {
    log_io: bool,
    pull: Option<PullOptions>,
    push: Option<PushOptions>,
}

impl RtmpOptions {
    pub fn new(log_io: bool, pull: Option<PullOptions>, push: Option<PushOptions>) -> RtmpOptions {
        RtmpOptions { log_io, push, pull }
    }
}

pub fn rtmp_server(options: &RtmpOptions) {
    let address = "0.0.0.0:1935".parse().unwrap();
    let listener = TcpListener::bind(&address).unwrap();
    let mut poll = Poll::new().unwrap();

    println!("Listening for connections");
    poll.register(&listener, SERVER, Ready::readable(), PollOpt::edge())
        .unwrap();

    let mut server = Server::new(&options.push);
    let mut connection_count = 1;
    let mut connections = Slab::new();

    // Setup pull connection
    if let Some(ref pull) = options.pull {
        println!(
            "Starting pull client for rtmp://{}/{}/{}",
            pull.host, pull.app, pull.stream
        );

        let mut pull_host = pull.host.clone();
        if !pull_host.contains(':') {
            pull_host += ":1935";
        }

        let addr = SocketAddr::from_str(&pull_host).unwrap();
        let stream = TcpStream::connect(&addr).unwrap();
        let mut connection = Connection::new(stream, connection_count, options.log_io, false);
        let token = connections.insert(connection);
        connection_count += 1;

        println!("Pull client started with connection id {}", token);
        connections[token].token = Some(Token(token));
        connections[token].register(&mut poll).unwrap();
        server.register_pull_client(
            token,
            pull.app.clone(),
            pull.stream.clone(),
            pull.target.clone(),
        );
    }

    let mut events = Events::with_capacity(1024);
    let mut outer_started_at = SystemTime::now();
    let mut inner_started_at;
    let mut total_ns = 0;
    let mut poll_count = 0_u32;

    loop {
        poll.poll(&mut events, None).unwrap();

        inner_started_at = SystemTime::now();
        poll_count += 1;

        for event in events.iter() {
            let mut connections_to_close = ClosedTokens::new();

            match event.token() {
                SERVER => {
                    let (socket, _) = listener.accept().unwrap();
                    let mut connection =
                        Connection::new(socket, connection_count, options.log_io, true);
                    let token = connections.insert(connection);

                    connection_count += 1;

                    println!("New connection (id {})", token);

                    connections[token].token = Some(Token(token));
                    connections[token].register(&mut poll).unwrap();
                }

                Token(token) => {
                    match process_event(event.readiness(), &mut connections, token, &mut poll) {
                        EventResult::None => (),
                        EventResult::ReadResult(result) => match *result {
                            ReadResult::HandshakingInProgess => (),
                            ReadResult::NoBytesReceived => (),
                            ReadResult::BytesReceived {
                                ref buffer,
                                byte_count,
                            } => {
                                connections_to_close = handle_read_bytes(
                                    &buffer[..byte_count],
                                    token,
                                    &mut server,
                                    &mut connections,
                                    &mut poll,
                                    &options,
                                    &mut connection_count,
                                );
                            }

                            ReadResult::HandshakeCompleted {
                                ref buffer,
                                byte_count,
                            } => {
                                connections_to_close = handle_read_bytes(
                                    &buffer[..byte_count],
                                    token,
                                    &mut server,
                                    &mut connections,
                                    &mut poll,
                                    &options,
                                    &mut connection_count,
                                )
                            }
                        },

                        EventResult::DisconnectConnection => {
                            connections_to_close.insert(token);
                        }
                    }
                }
            }
            for token in connections_to_close {
                println!("Closing connection id {}", token);
                connections.remove(token);
                server.notify_connection_closed(token);
            }
        }

        let inner_elapsed = inner_started_at.elapsed().unwrap();
        let outer_elapsed = outer_started_at.elapsed().unwrap();
        total_ns += inner_elapsed.subsec_nanos();

        if outer_elapsed.as_secs() >= 10 {
            let seconds_since_start = outer_started_at.elapsed().unwrap().as_secs();
            let seconds_doing_work =
                f64::from(total_ns) / f64::from(1000) / f64::from(1000) / f64::from(1000);
            let percentage_doing_work =
                (seconds_doing_work / seconds_since_start as f64) * f64::from(100);
            println!("Spent {} ms ({}% of time) doing work over {} seconds (avg {} microseconds per iteration)",
                     total_ns / 1000 / 1000,
                     percentage_doing_work as u32,
                     seconds_since_start,
                     (total_ns / poll_count) / 1000);

            // Reset so each notification is per that interval
            total_ns = 0;
            poll_count = 0;
            outer_started_at = SystemTime::now();
        }
    }
}

fn process_event(
    event: Ready,
    connections: &mut Slab<Connection>,
    token: usize,
    poll: &mut Poll,
) -> EventResult {
    let connection = match connections.get_mut(token) {
        Some(connection) => connection,
        None => return EventResult::None,
    };

    if event.is_writable() {
        match connection.writable(poll) {
            Ok(_) => (),
            Err(error) => {
                println!("Error occurred while writing: {:?}", error);
                return EventResult::DisconnectConnection;
            }
        }
    }

    if event.is_readable() {
        match connection.readable(poll) {
            Ok(result) => return EventResult::ReadResult(Box::new(result)),
            Err(ConnectionError::SocketClosed) => return EventResult::DisconnectConnection,
            Err(x) => {
                println!("Error occurred: {:?}", x);
                return EventResult::DisconnectConnection;
            }
        }
    }

    EventResult::None
}

fn handle_read_bytes(
    bytes: &[u8],
    from_token: usize,
    server: &mut Server,
    connections: &mut Slab<Connection>,
    poll: &mut Poll,
    app_options: &RtmpOptions,
    connection_count: &mut usize,
) -> ClosedTokens {
    let mut closed_tokens = ClosedTokens::new();

    let mut server_results = match server.bytes_received(from_token, bytes) {
        Ok(results) => results,
        Err(error) => {
            println!("Input caused the following server error: {}", error);
            closed_tokens.insert(from_token);
            return closed_tokens;
        }
    };

    for result in server_results.drain(..) {
        match result {
            ServerResult::OutboundPacket {
                target_connection_id,
                packet,
            } => if let Some(connection) = connections.get_mut(target_connection_id) {
                connection.enqueue_packet(poll, packet).unwrap()
            },

            ServerResult::DisconnectConnection { connection_id } => {
                closed_tokens.insert(connection_id);
            }

            ServerResult::StartPushing => {
                if let Some(ref push) = app_options.push {
                    println!(
                        "Starting push to rtmp://{}/{}/{}",
                        push.host, push.app, push.target_stream
                    );

                    let mut push_host = push.host.clone();
                    if !push_host.contains(':') {
                        push_host += ":1935";
                    }

                    let addr = SocketAddr::from_str(&push_host).unwrap();
                    let stream = TcpStream::connect(&addr).unwrap();
                    let mut connection =
                        Connection::new(stream, *connection_count, app_options.log_io, false);
                    let token = connections.insert(connection);
                    *connection_count += 1;

                    println!("Push client started with connection id {}", token);
                    connections[token].token = Some(Token(token));
                    connections[token].register(poll).unwrap();
                    server.register_push_client(token);
                }
            }
        }
    }

    closed_tokens
}
